﻿using System;
using System.Collections.Generic;

using Xunit;

namespace AppealsFunction.Tests
{
    public class FunctionTest
    {
        [Fact]
        public void TestOrdering()
        {
            List<Appeal> appeals = SqlClient.GetAppealsByStatusSortDescByDateBy(Status.PENDING);

            DateTime prev = DateTime.MaxValue;

            foreach (Appeal a in appeals)
            {
                Assert.True(a.DateBy <= prev);
                prev = a.DateBy;
            }
        }

        [Fact]
        public void TestValidation()
        {
            DateTime now = DateTime.UtcNow;

            Appeal a = new Appeal();
            a.DateSubmitted = now;
            a.DateBy = now.AddDays(7);

            //Description empty;
            Assert.False(ValidationUtil.ValidateAppeal(a));

            a.Description = "description";
            a.DateBy = now.AddDays(-7);

            //DateBy in the past
            Assert.False(ValidationUtil.ValidateAppeal(a));

            a.DateBy = now.AddDays(7);

            Assert.True(ValidationUtil.ValidateAppeal(a));
        }
    }
}