﻿using System;

namespace AppealsFunction
{
    public class Appeal
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public DateTime DateSubmitted { get; set; }

        public DateTime DateBy { get; set; }

        public Status Status { get; set; }
    }
}