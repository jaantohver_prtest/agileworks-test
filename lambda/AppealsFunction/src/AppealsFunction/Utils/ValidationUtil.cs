﻿using System;

namespace AppealsFunction
{
    public static class ValidationUtil
    {
        public static bool ValidateAppeal(Appeal appeal)
        {
            if (appeal.DateBy < DateTime.UtcNow)
            {
                return false;
            }

            if (string.IsNullOrWhiteSpace(appeal.Description))
            {
                return false;
            }

            return true;
        }
    }
}