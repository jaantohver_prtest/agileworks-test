'use strict';

// var url = "http://localhost:5000/api/appeals";
var url = "https://koyefmtoq5.execute-api.eu-west-1.amazonaws.com/dev/api/appeals"

document.addEventListener('DOMContentLoaded', function() {
  var form = document.getElementById("appeal-form");

  if (form) {
    form.addEventListener("submit", function (event) {
      event.preventDefault();

      postAppeal(form);
    });
  }
});

function postAppeal(form) {
  var XHR = new XMLHttpRequest();
  var FD = new FormData(form);

  var object = {};
  FD.forEach(function(value, key){
    object[key] = value;
  });
  var json = JSON.stringify(object);

  fetch(url, {
    method: "POST",
    mode: "cors",
    headers: {
      "Content-Type": "application/json",
    },
    body: json
  }).then(data => {
    var snack = document.getElementById("snackbar");

    if (data.status == 200) {
      snack.innerHTML = "Appeal submitted"
    } else {
      snack.innerHTML = "Bad request"
    }

    snack.className = "show";
    setTimeout(function(){ snack.className = snack.className.replace("show", ""); }, 2000);
  }).catch(error => {
    snack.innerHTML = "Bad request"
    
    console.log(error);
  })
}