'use strict';

// var urlBase = "http://localhost:5000/api/appeals";
var urlBase = "https://koyefmtoq5.execute-api.eu-west-1.amazonaws.com/dev/api/appeals"

document.addEventListener('DOMContentLoaded', function() {
  var form = document.getElementById("contactForm");

  if (form) {
    form.addEventListener("submit", function (event) {
      event.preventDefault();

      sendContactMail(form);
    });
  }
});

window.addEventListener("load", function () {
  getPendingAppeals()
});

function getPendingAppeals() {  
  var url = urlBase + "/pending";

  fetch(url)
  .then(data => {
    return data.json();
  }).then(json => {
    console.log(json);
    populateList(json);
  }).catch(error => {
    document.getElementById('message').style.cssText = "display: default;"
    document.getElementById('message').innerHTML = error;
  });
}

function populateList(appeals) {
  if (appeals.length == 0) {
    document.getElementById('message').innerHTML = "No pending appeals.";
  } else {
    document.getElementById('list-header').style.cssText = "display: default;"
    document.getElementById('message').style.cssText = "display: none;"
    var container = document.getElementById("elementContainer")
    var template = document.getElementsByTagName("template")[0];
    var content = template.content.querySelector("div");

    for (var i = 0; i < appeals.length; i++) {
      var appeal = appeals[i]

      //FIXME js by default sets the timezone to local, even though the server sends back utc time
      var submitted = new Date(appeal['dateSubmitted']);
      var by = new Date(appeal['dateBy']);
      
      var item = content.cloneNode(true);
      var description = item.querySelector("#description");
      var dateSubmitted = item.querySelector("#date-submitted");
      var dateBy = item.querySelector("#date-by");
      var resolveButton = item.querySelector("#solve");

      description.textContent = appeal['description']
      dateSubmitted.textContent = formatDate(submitted);
      dateBy.textContent = formatDate(by);
      resolveButton.id = resolveButton.id + " " + appeal['id'];

      resolveButton.addEventListener('click', function(event) {
        event.preventDefault();
        
        solve(event.target);
      });

      if (by <= new Date().addHours(1)) {
        item.firstChild.style.backgroundColor = "#ee5253";
      }
      
      container.appendChild(item)
    }
  }
}

function formatDate(date) {
  return padWithZeroes(date.getDate()) + "." + 
         padWithZeroes(date.getMonth()) + "." + 
         date.getFullYear() + " " + 
         padWithZeroes(date.getHours()) + ":" + 
         padWithZeroes(date.getMinutes());
}

function padWithZeroes(dateString) {
  return String(dateString).padStart(2, '0')
}

function solve(item) {
  var id = item.id.split(" ")[1]
  var url = urlBase + "/" + id
  
  fetch(url, {
    method: "POST",
    mode: "cors"
  }).then(data => {
    console.log(data);
    return data.text();
  }).then(json => {
    console.log(json);
    item.parentNode.parentNode.style.display = "none";

    var snack = document.getElementById("snackbar");
    snack.className = "show";
    setTimeout(function(){ snack.className = snack.className.replace("show", ""); }, 2000);
  }).catch(error => {
    console.log(error);
  })
}

Date.prototype.addHours = function(h) {    
  this.setTime(this.getTime() + (h*60*60*1000)); 
  return this;   
}