'use strict';
const gulp = require('gulp');
const pug = require('gulp-pug');
const less = require('gulp-less');
const cssnano = require('gulp-cssnano');
const htmlMinifier = require('gulp-html-minifier');
const imagemin = require('gulp-imagemin');
const del = require('del');
const gulpif = require('gulp-if');

gulp.task('views', () => {
    return gulp.src('frontend_src/pug/*.pug')
        .pipe(pug({}))
        .pipe(gulp.dest('dist/'));
});

gulp.task('html', () => {
  return gulp.src('frontend_src/pug/*.pug')
  .pipe(pug({}))
  .pipe(htmlMinifier({
    removeComments: true,
    collapseWhitespace: true,
    removeTagWhitespace: true
  }))
  .pipe(gulp.dest('dist/'));
});

gulp.task('images', () =>
  gulp.src('frontend_src/media/**/*')
  .pipe(imagemin())
  .pipe(gulp.dest('dist/media/'))
);

gulp.task('less', () => {
  return gulp.src('frontend_src/less/*.less')
  .pipe(less())
  .pipe(gulp.dest('dist/css/'));
});

gulp.task('copy', () => {
  return gulp.src('frontend_src/dist/**/*')
  .pipe(gulpif(/[.]css$/, cssnano({
    discardComments: {removeAll: true}
  })))
  .pipe(gulp.dest('dist/'));
});

gulp.task('css', () => {
  return gulp.src('frontend_src/less/*.less')
  .pipe(less())
  .pipe(cssnano())
  .pipe(gulp.dest('dist/css'));
});

gulp.task('clean', (cb) => {
  return del.sync('dist',cb);
});

gulp.task('watch', () => {
  return gulp.watch('frontend_src/**/*', ['views', 'css', 'images', 'copy']);
});

gulp.task('build', ['views', 'css', 'images', 'copy']);
gulp.task('cleanandbuild', ['clean', 'build']);
gulp.task('default', ['cleanandbuild']);
